install:
	curl -sS https://get.symfony.com/cli/installer | bash
	export PATH="$HOME/.symfony/bin:$PATH"
	sudo mv /home/u/.symfony/bin/symfony /usr/local/bin/symfony
	symfony -V
start:
	docker-compose start
stop:
	docker-compose stop
reset:
	docker-compose stop
	docker-compose start
ps:
	docker-compose ps
bash:
	docker-compose exec php /bin/bash