<?php

namespace App\Entity;

use App\Repository\QuestionRepository;
use DateTimeImmutable;
use Doctrine\ORM\Mapping as ORM;

/**
 * @ORM\Entity(repositoryClass=QuestionRepository::class)
 */
class Question
{
    /**
     * @ORM\Id
     * @ORM\GeneratedValue
     * @ORM\Column(type="integer")
     */
    private int $id;

    /** @ORM\Column(type="string", length=255) */
    private string $name;

    /** @ORM\Column(type="string", length=255) */
    private string $slug;

    /** @ORM\Column(type="datetime", name="asked_at") */
    private DateTimeImmutable $askedAt;


    /** @ORM\Column(type="string", length=255) */
    private string $question;

    public function getId(): ?int
    {
        return $this->id;
    }

    public function getName(): string
    {
        return $this->name;
    }

    public function getSlug(): string
    {
        return $this->slug;
    }

    public function getAskedAt(): string
    {
        return $this->askedAt;
    }

    public function getQuestion(): string
    {
        return $this->question;
    }
}
