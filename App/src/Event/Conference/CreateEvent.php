<?php

namespace App\Event\Conference;

use App\Entity\Conference;
use Symfony\Contracts\EventDispatcher\Event;

class CreateEvent extends Event
{
    public function __construct(protected Conference $conference){}

    public function getConference(): Conference
    {
        return $this->conference;
    }
}