<?php

namespace App\Actions;

use Psr\Log\LoggerInterface;
use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Symfony\Component\HttpFoundation\JsonResponse;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\Routing\Annotation\Route;

class QuestionController extends AbstractController
{
    /**
     * @Route("/homepage", name="")
    */
    public function homepage()
    {
        return new Response('Bla bla Bla');
    }

    /**
     * @Route("/homepage/logger")
    */
    public function logger(LoggerInterface $logger)
    {
        $logger->error('Error error error !!!');
        $logger->info('You are geu!');

        return $this->render('question/show.html.twig', [
            'title' => 'Loger',
            'questions' => [
                [
                    'title' => 'question 1',
                    'body' => 'body 1',
                ],
            ]
        ]);
    }

    /**
     * @Route("/homepage/{slug}/tt")
     */
    public function showtt($slug)
    {
        $response = new JsonResponse([
            'title' => $slug,
            'questions' => [
                [
                    'title' => 'question 1',
                    'body' => 'body 1',
                ],
                [
                    'title' => 'question 2',
                    'body' => 'body 2',
                ],
            ]
        ]);

        dd($response);
    }

    /**
     * @Route("/homepage/{slug}/showtest")
     */
    public function showtest($slug)
    {
        return $this->render('question/showtest.html.twig', [
            'title' => $slug,
            'questions' => [
                [
                    'title' => 'question 1',
                    'body' => 'body 1',
                ],
                [
                    'title' => 'question 2',
                    'body' => 'body 2',
                ],
            ]
        ]);
    }
}