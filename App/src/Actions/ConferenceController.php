<?php

namespace App\Actions;

use App\Entity\Comment;
use App\Entity\Conference;
use App\Event\Conference\CreateEvent;
use App\Form\Conference\Create;
use App\Listener\Conference\ConferenceListener;
use Doctrine\ORM\EntityManagerInterface;
use Psr\EventDispatcher\EventDispatcherInterface;
use Symfony\Component\Form\FormFactoryInterface;
use Symfony\Component\HttpFoundation\JsonResponse;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Response;

class ConferenceController
{
    public function __construct(
        private FormFactoryInterface $formFactory,
        private EventDispatcherInterface $eventDispatcher,
        private EntityManagerInterface $entityManager,
    ) {}

    public function create(Request $request): JsonResponse
    {
        $form = $this->formFactory->create(Create::class)
            ->submit($request->request->all())
            ->handleRequest($request);

        if (!$form->isSubmitted() || !$form->isValid()) {
            return new JsonResponse([
                'errors' => $form->getData()
            ]);
        }

        $data = $form->getData();

        $conference = new Conference();
        $conference->setCity($data['city']);
        $conference->setYear($data['year']);
        $conference->setIsInternational($data['is_international']);

        $this->entityManager->persist($conference);
        $this->entityManager->flush($conference);

        $this->eventDispatcher->dispatch(new CreateEvent($conference));

        return new JsonResponse([], Response::HTTP_NO_CONTENT);
    }
}