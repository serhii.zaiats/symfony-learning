<?php

namespace App\Form\Conference;

use Symfony\Component\Form\AbstractType;
use Symfony\Component\Form\Extension\Core\Type\TextType;
use Symfony\Component\Form\FormBuilderInterface;
use Symfony\Component\Validator\Constraints\Length;
use Symfony\Component\Validator\Constraints\Range;

class Create extends AbstractType
{
    public function buildForm(FormBuilderInterface $builder, array $options): void
    {
        $builder
            ->add('city', TextType::class, [
                'required' => true,
                'constraints' => [new Length(['min' => 3])]
            ])
            ->add('year', TextType::class, [
                'required' => true,
                'constraints' => [new Length(['max' => 4])]
            ])
            ->add('is_international', TextType::class, [
                'required' => true,
                'constraints' => [new Range(['min' => 0, 'max' => 1])]
            ])
        ;
    }
}