<?php

namespace App\Listener\Conference;

use App\Entity\Comment;
use App\Event\Conference\CreateEvent;
use Doctrine\ORM\EntityManagerInterface;
use Symfony\Component\EventDispatcher\EventSubscriberInterface;

class ConferenceListener implements EventSubscriberInterface
{
    public function __construct(protected EntityManagerInterface $entityManager){}

    public static function getSubscribedEvents()
    {
        return [
              CreateEvent::class => 'onCreate'
        ];
    }

    public function onCreate(CreateEvent $event): void
    {
        throw new \Exception('pppppppppppp123ppp');

        $conference = $event->getConference();

        $comment = new Comment();
        $comment->setAuthor('System');
        $comment->setText('Init conference');
        $comment->setEmail('test@email.com');
        $comment->setCreatedAt(new \DateTimeImmutable());
        $comment->setConference($conference);
        $this->entityManager->persist($comment);
        $this->entityManager->flush($comment);

        $conference->addComment($comment);
        $this->entityManager->persist($conference);
        $this->entityManager->flush($conference);
    }
}