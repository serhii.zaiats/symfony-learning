<?php

use App\Actions\ConferenceController;
use Symfony\Component\Routing\Loader\Configurator\RoutingConfigurator;

return function (RoutingConfigurator $routes) {
    $routes->add('conference.create', '/conference/create')
        ->controller([ConferenceController::class, 'create'])
        ->methods(['POST'])
    ;
};
